<?php

    spl_autoload_register(function ($class_name) {
        require $class_name .'.php';
    });

    $sheep = new Animal("shaun");

    echo "Nama: " . $sheep->get_name();
    echo "<br>";
    echo "Jumlah kaki: " . $sheep->get_legs();
    echo "<br>";
    echo "Berdarah dingin: ";
    echo $sheep->get_cold_blooded() ? 'true' : 'false';
    echo "<br><br>";

    $kodok = new Frog("buduk");
    $kodok -> jump();
    echo "<br>";
    echo "Nama: " . $kodok -> get_name();
    echo "<br>";
    echo "Jumlah kaki: " . $kodok -> get_legs();
    echo "<br>";
    echo "Berdarah dingin: ";
    echo $kodok->get_cold_blooded() ? 'true' : 'false';
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");
    $sungokong->yell();
    echo "<br>";
    echo "Nama: " . $sungokong->get_name();
    echo "<br>";
    echo "Jumlah kaki: " . $sungokong->get_legs();
    echo "<br>";
    echo "Berdarah dingin: ";
    echo $sungokong->get_cold_blooded() ? 'true' : 'false';


?>